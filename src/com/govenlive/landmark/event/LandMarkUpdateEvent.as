package com.govenlive.landmark.event
{
    
    import com.govenlive.landmark.datas.Face_landmark;
    
    import flash.events.Event;
    
    
    /**
     *@author 偷心枫贼
     *        下午4:41:27
     *
     */
    public class LandMarkUpdateEvent extends Event
    {
        public static const UPDATE_LANDMARK:String="update_landmark"
            
            
        public var session_id:String=""
        public var face_landmark:Face_landmark=null;
        
        public function LandMarkUpdateEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false,$face_landmark:Face_landmark=null,$session_id:String="")
        {
            this.session_id=$session_id;
            this.face_landmark=$face_landmark;
            super(type, bubbles, cancelable);
        }
    }
}