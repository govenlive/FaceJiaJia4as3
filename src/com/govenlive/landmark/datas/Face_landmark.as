package com.govenlive.landmark.datas
{
    
    
    /**
    * 脸部
     *@author 偷心枫贼
     *        下午3:27:40
     *
     */
    public class Face_landmark
    {
        
        private var _face_id:String=""
        private var _landmark:LandMark=null;
                
        /**
         * 包含详细关键点分析结果，包含多个关键点的坐标。 
         * @return 
         * 
         */        
        public function get landmark():LandMark{
            return _landmark;
        }
            
        /**
         * 人脸在Face++系统中的标识符 
         * @return 
         * 
         */            
        public function get face_id():String{
            return _face_id;
        }
            
        public function Face_landmark()
        {
        }
        
        
        public function copyObject(obj:Object):void{
            _face_id=obj.face_id;
            _landmark=new LandMark()
            _landmark.copyObject(obj.landmark);
        }
        
        
        public function toJson():String{
            return JSON.stringify(this);
        }
    }
}