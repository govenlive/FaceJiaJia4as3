package com.govenlive.landmark.datas
{
    
    
    /**
    * 关键点信息
     *@author 偷心枫贼
     *        下午3:29:18
     *
     */
    public class KeyPoint
    {
        
        public var x:Number=NaN;
        public var y:Number=NaN;
        public var name:String=""
            
        public function KeyPoint()
        {
        }
    }
}