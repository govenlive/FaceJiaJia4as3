package com.govenlive.landmark.datas
{
    
    
    
    /**
    * 关键点集合
     *@author 偷心枫贼
     *        下午3:24:48
     *
     */
    public class LandMark
    {
        /**下巴*/
        public static const contour_chin:String="CNTOUR_CHIN";
        /**左边第1个点*/
        public static const contour_left1:String="CONTOUR_LEFT1";
        /**左边第2个点*/
        public static const contour_left2:String="CONTOUR_LEFT2";
        /**左边第3个点*/        
        public static const contour_left3:String="CONTOUR_LEFT3";
        /**左边第4个点*/
        public static const contour_left4:String="CONTOUR_LEFT4";
        /**左边第5个点*/
        public static const contour_left5:String="CONTOUR_LEFT5";
        /**左边第6个点*/
        public static const contour_left6:String="CONTOUR_LEFT6";
        /**左边第7个点*/
        public static const contour_left7:String="CONTOUR_LEFT7";
        /**左边第8个点*/
        public static const contour_left8:String="CONTOUR_LEFT8";
        /**左边第9个点*/
        public static const contour_left9:String="CONTOUR_LEFT9";
        
        /**右边第1个点*/
        public static const contour_right1:String="CONTOUR_RIGHT1";
        /**右边第2个点*/
        public static const contour_right2:String="CONTOUR_RIGHT2";
        /**右边第3个点*/
        public static const contour_right3:String="CONTOUR_RIGHT3";
        /**右边第4个点*/
        public static const contour_right4:String="CONTOUR_RIGHT4";
        /**右边第5个点*/
        public static const contour_right5:String="CONTOUR_RIGHT5";
        /**右边第6个点*/
        public static const contour_right6:String="CONTOUR_RIGHT6";
        /**右边第7个点*/
        public static const contour_right7:String="CONTOUR_RIGHT7";
        /**右边第8个点*/
        public static const contour_right8:String="CONTOUR_RIGHT8";
        /**右边第9个点*/
        public static const contour_right9:String="CONTOUR_RIGHT9";
                
        /**左眼上*/
        public static const left_eye_top:String="LEFT_EYE_TOP";
        /**左眼下*/
        public static const left_eye_bottom:String="LEFT_EYE_BOTTOM";
        /**左眼左*/        
        public static const left_eye_left_corner:String="LEFT_EYE_LEFT_CORNER";
        /**左眼右*/  
        public static const left_eye_right_corner:String="LEFT_EYE_RIGHT_CORNER";     
        /**左眼中*/
        public static const left_eye_center:String="LEFT_EYE_CENTER";
        /**左眼瞳孔*/
        public static const left_eye_pupil:String="LEFT_EYE_PUPIL";     
                
        /**左眉左角落*/
        public static const left_eyebrow_left_corner:String="LEFT_EYEBROW_LEFT_CORNER";
        /**左眉右角落*/
        public static const left_eyebrow_right_corner:String="LEFT_EYEBROW_RIGHT_CORNER";
        
        /**左眼左上角*/
        public static const left_eye_upper_left_quarter:String="LEFT_EYE_UPPER_LEFT_QUARTER";
        /**左眼左下角*/
        public static const left_eye_lower_left_quarter:String="LEFT_EYE_LOWER_LEFT_QUARTER";
        /**左眼右上角*/
        public static const left_eye_upper_right_quarter:String="LEFT_EYE_UPPER_RIGHT_QUARTER";
        /**左眼右下角*/
        public static const left_eye_lower_right_quarter:String="LEFT_EYE_LOWER_RIGHT_QUARTER";
            
        /**左眉中下*/
        public static const left_eyebrow_lower_middle:String="LEFT_EYEBROW_LOWER_MIDDLE";
        /**左眉中上*/
        public static const left_eyebrow_upper_middle:String="LEFT_EYEBROW_UPPER_MIDDLE";
        /**左眉左*/
        public static const left_eyebrow_upper_left_quarter:String="LEFT_EYEBROW_UPPER_LEFT_QUARTER";  
        /**左眉右*/
        public static const left_eyebrow_upper_right_quarter:String="LEFT_EYEBROW_UPPER_RIGHT_QUARTER"; 
        /**左眉左下角*/
        public static const left_eyebrow_lower_left_quarter:String="LEFT_EYEBROW_LOWER_LEFT_QUARTER";
        /**左眉右下角*/
        public static const left_eyebrow_lower_right_quarter:String="LEFT_EYEBROW_LOWER_RIGHT_QUARTER";
              
        /**右眼上*/
        public static const right_eye_top:String="RIGHT_EYE_TOP";
        /**右眼下*/
        public static const right_eye_bottom:String="RIGHT_EYE_BOTTOM";
        /**右眼左*/
        public static const right_eye_left_corner:String="RIGHT_EYE_LEFT_CORNER";
        /**右眼右*/
        public static const right_eye_right_corner:String="RIGHT_EYE_RIGHT_CORNER";        
        /**右眼中*/
        public static const right_eye_center:String="RIGHT_EYE_CENTER";
        /**右眼瞳孔*/
        public static const right_eye_pupil:String="RIGHT_EYE_PUPIL";
          
        /**右眼左下角*/
        public static const right_eye_lower_left_quarter:String="RIGHT_EYE_LOWER_LEFT_QUARTER";        
        /**右眼右下角*/
        public static const right_eye_lower_right_quarter:String="RIGHT_EYE_LOWER_RIGHT_QUARTER";       
        /**右眼左上角*/
        public static const right_eye_upper_left_quarter:String="RIGHT_EYE_UPPER_LEFT_QUARTER";
        /**右眼右上角*/
        public static const right_eye_upper_right_quarter:String="RIGHT_EYE_UPPER_RIGHT_QUARTER";
        
        /**右眉左*/
        public static const right_eyebrow_left_corner:String="RIGHT_EYEBROW_LEFT_CORNER";
        /**右眉右*/
        public static const right_eyebrow_right_corner:String="RIGHT_EYEBROW_RIGHT_CORNER";
        /**右眉中下*/
        public static const right_eyebrow_lower_middle:String="RIGHT_EYEBROW_LOWER_MIDDLE";
        /**右眉中上*/
        public static const right_eyebrow_upper_middle:String="RIGHT_EYEBROW_UPPER_MIDDLE";   
        /**右眉左下角*/
        public static const right_eyebrow_lower_left_quarter:String="RIGHT_EYEBROW_LOWER_LEFT_QUARTER";
        /**右眉右下角*/
        public static const right_eyebrow_lower_right_quarter:String="RIGHT_EYEBROW_LOWER_RIGHT_QUARTER";
        /**右眉左上角*/
        public static const right_eyebrow_upper_left_quarter:String="RIGHT_EYEBROW_UPPER_LEFT_QUARTER";
        /**右眉右上角*/
        public static const right_eyebrow_upper_right_quarter:String="RIGHT_EYEBROW_UPPER_RIGHT_QUARTER";
        
        /**左嘴角*/
        public static const mouth_left_corner:String="MOUTH_LEFT_CORNER";
        /**右嘴角*/
        public static const mouth_right_corner:String="MOUTH_RIGHT_CORNER";
        /**下嘴唇底*/
        public static const mouth_lower_lip_bottom:String="MOUTH_LOWER_LIP_BOTTOM";     
        /**下嘴唇顶*/
        public static const mouth_lower_lip_top:String="MOUTH_LOWER_LIP_TOP";
        /**上嘴唇底*/
        public static const mouth_upper_lip_bottom:String="MOUTH_UPPER_LIP_BOTTOM";
        /**上嘴唇顶*/
        public static const mouth_upper_lip_top:String="MOUTH_UPPER_LIP_TOP";
        
        /**下嘴唇左1*/
        public static const mouth_lower_lip_left_contour1:String="MOUTH_LOWER_LIP_LEFT_CONTOUR1";
        /**下嘴唇左2*/
        public static const mouth_lower_lip_left_contour2:String="MOUTH_LOWER_LIP_LEFT_CONTOUR2";
        /**下嘴唇左3*/
        public static const mouth_lower_lip_left_contour3:String="MOUTH_LOWER_LIP_LEFT_CONTOUR3";
        /**下嘴唇右1*/
        public static const mouth_lower_lip_right_contour1:String="MOUTH_LOWER_LIP_RIGHT_CONTOUR1";
        /**下嘴唇右2*/
        public static const mouth_lower_lip_right_contour2:String="MOUTH_LOWER_LIP_RIGHT_CONTOUR2";
        /**下嘴唇右3*/
        public static const mouth_lower_lip_right_contour3:String="MOUTH_LOWER_LIP_RIGHT_CONTOUR3";
        /**上嘴唇左1*/
        public static const mouth_upper_lip_left_contour1:String="MOUTH_UPPER_LIP_LEFT_CONTOUR1";
        /**上嘴唇左2*/
        public static const mouth_upper_lip_left_contour2:String="MOUTH_UPPER_LIP_LEFT_CONTOUR2";
        /**上嘴唇左3*/
        public static const mouth_upper_lip_left_contour3:String="MOUTH_UPPER_LIP_LEFT_CONTOUR3";
        /**上嘴唇右1*/
        public static const mouth_upper_lip_right_contour1:String="MOUTH_UPPER_LIP_RIGHT_CONTOUR1";
        /**上嘴唇右2*/
        public static const mouth_upper_lip_right_contour2:String="MOUTH_UPPER_LIP_RIGHT_CONTOUR2";
        /**上嘴唇右3*/
        public static const mouth_upper_lip_right_contour3:String="MOUTH_UPPER_LIP_RIGHT_CONTOUR3";
        
      
        /**鼻子左*/
        public static const nose_left:String="NOSE_LEFT";
        /**鼻子右*/
        public static const nose_right:String="NOSE_RIGHT";
        /**鼻子中*/
        public static const nose_tip:String="NOSE_TIP";
        /**鼻子底中*/
        public static const nose_contour_lower_middle:String="NOSE_CONTOUR_LOWER_MIDDLE";
        
        /**鼻子左1*/
        public static const nose_contour_left1:String="NOSE_CONTOUR_LEFT1";
        /**鼻子左2*/
        public static const nose_contour_left2:String="NOSE_CONTOUR_LEFT2";
        /**鼻子左3*/
        public static const nose_contour_left3:String="NOSE_CONTOUR_LEFT3";
        
        /**鼻子右1*/
        public static const nose_contour_right1:String="NOSE_CONTOUR_RIGHT1";
        /**鼻子右2*/
        public static const nose_contour_right2:String="NOSE_CONTOUR_RIGHT2";
        /**鼻子右3*/
        public static const nose_contour_right3:String="NOSE_CONTOUR_RIGHT3";
        
       
        
        
        
        
        
        
        
        private var _keyPoints:Vector.<KeyPoint>=null;
        
        
        
        /**
         * 关键点集合 
         * @return 
         * 
         */        
        public function get keyPoints():Vector.<KeyPoint>{return _keyPoints}
        
        /**
         * 获取关键点 
         * @param pointName 根据相应的名字
         * @return 
         * 
         */        
        public function getKeyPoint(pointName:String):KeyPoint{
            if(keyPoints==null)return null;
            for each(var keyPoint:KeyPoint in keyPoints){
                if(keyPoint.name==pointName)return keyPoint
            }
            return null 
        }
        
        
        
            
        public function LandMark()
        {
        }
        
        public function copyObject(obj:Object):void{
            _keyPoints=new Vector.<KeyPoint>()
            
            for(var i:String in obj){
                var pObj:Object=obj[i];
                var keyPoint: KeyPoint=new  KeyPoint()
                keyPoint.x=pObj.x
                keyPoint.y=pObj.y
                keyPoint.name=i.toLocaleUpperCase();
              
                keyPoints.push(keyPoint);
            }
        }
    }
}