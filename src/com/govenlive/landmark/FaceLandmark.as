package com.govenlive.landmark
{
    import com.govenlive.landmark.datas.Face_landmark;
    import com.govenlive.landmark.event.LandMarkUpdateEvent;
    import com.govenlive.landmark.loaders.ILoaderFaceLandMark;
    import com.govenlive.landmark.loaders.LoaderFaceLandMarkFromFace_id;
    import com.govenlive.landmark.loaders.LoaderFaceLandMarkFromImg;
    import com.govenlive.landmark.types.PType;
    
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.IOErrorEvent;
    
    [Event(name="update_landmark", type="com.govenlive.landmark.event.LandMarkUpdateEvent")]
    [Event(name="ioError", type="flash.events.IOErrorEvent")]
    /**
    * 检测给定人脸相应的面部轮廓，五官等关键点的位置
     *@author 偷心枫贼
     *        下午3:00:17
     *
     */
    public class FaceLandmark extends EventDispatcher
    {
        private static var _instance:FaceLandmark=null
        
            
        /**
         * 实现单例模式 
         * @return 
         * 
         */            
        public static function get instance():FaceLandmark{
            if(_instance==null){
                _instance=new FaceLandmark()
            }
            
            return _instance;
        }
        
        public var max_leng:uint=100;
        
        
        private var _loaders:Vector.<ILoaderFaceLandMark>=new Vector.<ILoaderFaceLandMark>()
            
           
        
        
       
        public function FaceLandmark()
        {
        }
        
        /**
         * 检测
         * @param face_id 待检测人脸的face_id
         * @param type 表示返回的关键点个数，目前支持83p或25p，默认为25p
         */        
        public function detect_faceId(face_id:String,type:String=PType.P25):void{
            if(_loaders.length<max_leng){
                var iloader:ILoaderFaceLandMark=new LoaderFaceLandMarkFromFace_id();
                iloader.addEventListener(Event.COMPLETE,onComplete)
                iloader.addEventListener(IOErrorEvent.IO_ERROR,onComplete)
                _loaders.push(iloader);
                iloader.detect_faceId(face_id,type)
            }
          
        }
        
        
        /**
         *  
         * @param img 图片链接或者 位图数据
         * @param type 表示返回的关键点个数，目前支持83p或25p，默认为83p
         * 
         */        
        public function detect_bitm(img:*,type:String=PType.P25):void{
            if(_loaders.length<max_leng){
                var iloader:ILoaderFaceLandMark=new  LoaderFaceLandMarkFromImg()
                iloader.addEventListener(Event.COMPLETE,onComplete)
                iloader.addEventListener(IOErrorEvent.IO_ERROR,onComplete)
                _loaders.push(iloader);
                iloader.detect_img(img,type)
            }
       
        }
        
       
        private function onComplete(e:Event):void{
            var loader:ILoaderFaceLandMark=e.target as ILoaderFaceLandMark
            loader.removeEventListener(Event.COMPLETE,onComplete)
            loader.removeEventListener(IOErrorEvent.IO_ERROR,onComplete)
            _loaders.splice(_loaders.indexOf(loader),1)
            if(e.type==Event.COMPLETE){
                var obj:Object=JSON.parse(loader.json)
                var session_id:String=obj.session_id;
                var arr:Array=obj.result;
                if(arr.length>0){
                    var result:Object=arr[0]
                    var face_landmark:Face_landmark=new Face_landmark();
                    face_landmark.copyObject(result);
                  //  trace(face_landmark.toJson())
                    var event:LandMarkUpdateEvent=new LandMarkUpdateEvent(LandMarkUpdateEvent.UPDATE_LANDMARK,false,false,face_landmark,session_id)
                    dispatchEvent(event);   
                }
            }else{
                //trace("error");
            }
            
            loader=null;
        }
        
    }
}

