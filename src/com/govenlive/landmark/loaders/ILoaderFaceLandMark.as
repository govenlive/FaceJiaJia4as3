package com.govenlive.landmark.loaders
{
    
    import com.govenlive.landmark.types.PType;
    
    import flash.events.IEventDispatcher;
    
    
    /**
     *@author 偷心枫贼
     *        下午10:43:55
     *
     */
    public interface ILoaderFaceLandMark extends IEventDispatcher
    {
        function detect_faceId(face_id:String,type:String=PType.P25):void
        function detect_img(img:*,type:String=PType.P25):void
        function get json():String
    }
}