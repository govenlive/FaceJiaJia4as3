package com.govenlive.landmark.loaders
{
    
    import com.govenlive.detect.Loader_faceDetect;
    import com.govenlive.detect.datas.Face;
    import com.govenlive.detect.event.DetectUpdateEvent;
    import com.govenlive.detect.types.AttributeType;
    import com.govenlive.detect.types.ModeType;
    import com.govenlive.landmark.types.PType;
    
    import flash.events.Event;
    import flash.events.IEventDispatcher;
    import flash.events.IOErrorEvent;
    
    
    /**
     *@author 偷心枫贼
     *        下午11:03:26
     *
     */
    public class LoaderFaceLandMarkFromImg extends _LoaderFace
    {
        public function LoaderFaceLandMarkFromImg(target:IEventDispatcher=null)
        {
            super(target);
            
            Loader_faceDetect.instance.addEventListener(DetectUpdateEvent.UPDATE_DETECT,onUpdateDetect)
            Loader_faceDetect.instance.addEventListener(IOErrorEvent.IO_ERROR,onUpdateDetect)
        }
        
        override public function detect_img(img:*, type:String=PType.P25):void
        {
            // TODO Auto Generated method stub
            super.detect_img(img, type);
            
            Loader_faceDetect.instance.detect(img,ModeType.NORMAL,AttributeType.NORMAL_LABELS,"",false)
                
        }
        
       
        
        private function onUpdateDetect(e:DetectUpdateEvent):void{     
            Loader_faceDetect.instance.removeEventListener(DetectUpdateEvent.UPDATE_DETECT,onUpdateDetect)
            Loader_faceDetect.instance.removeEventListener(IOErrorEvent.IO_ERROR,onUpdateDetect)
               
            for each(var face:Face in e.faces){
                var iloader:ILoaderFaceLandMark=new LoaderFaceLandMarkFromFace_id()
                 iloader.addEventListener(Event.COMPLETE,onComplete2)
                 iloader.addEventListener(IOErrorEvent.IO_ERROR,onComplete2)
                     trace(face.face_id)
                 iloader.detect_faceId(face.face_id,_type);
            }
        }
        
        
        private function onComplete2(e:Event):void{
            var iloader:ILoaderFaceLandMark=e.target as ILoaderFaceLandMark
            iloader.removeEventListener(Event.COMPLETE,onComplete2)
            iloader.removeEventListener(IOErrorEvent.IO_ERROR,onComplete2)
                
              //  trace(e.type)
            if(e.type==Event.COMPLETE){
               // trace(loader.json)
                _json=iloader.json
            }
 
            iloader=null
                
            dispatchEvent(e)
        }
        
    }
}