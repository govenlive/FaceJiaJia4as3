package com.govenlive.landmark.loaders
{
    
    import com.govenlive.landmark.types.PType;
    
    import flash.events.IEventDispatcher;
    
    
    /**
     *@author 偷心枫贼
     *        下午10:47:27
     *
     */
    public class LoaderFaceLandMarkFromFace_id extends _LoaderFace
    {
        public function LoaderFaceLandMarkFromFace_id(target:IEventDispatcher=null)
        {
            super(target);
        }
        
        override public function detect_faceId(face_id:String, type:String=PType.P25):void
        {
            // TODO Auto Generated method stub
            super.detect_faceId(face_id, type);
            variables.face_id=face_id
            req.data=variables
            
            loader.load(req); 
        }
        

        
    }
}