package com.govenlive.landmark.loaders
{
    
    import com.govenlive.FaceLibConfig;
    import com.govenlive._g.UrlTypes;
    import com.govenlive.landmark.types.PType;
    
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.IEventDispatcher;
    import flash.events.IOErrorEvent;
    import flash.net.URLLoader;
    import flash.net.URLRequest;
    import flash.net.URLVariables;
    
    
    /**
     *@author 偷心枫贼
     *        下午10:44:23
     *
     */
    public class _LoaderFace extends EventDispatcher implements ILoaderFaceLandMark
    {
        protected var variables:URLVariables = new URLVariables(); 
        protected var req:URLRequest=new URLRequest();
        protected var loader:URLLoader=new URLLoader()
        protected var _type:String="";
        protected var _json:String;
        
        
        
        
        
       
        
        public function get json():String
        {
            // TODO Auto Generated method stub
            return _json
        }
        
        
        public function _LoaderFace(target:IEventDispatcher=null)
        {
            super(target);
            
            variables.api_key=FaceLibConfig.api_key
            variables.api_secret=FaceLibConfig.api_secret
          
            req.url=UrlTypes.LANDMARK_URL
                
            loader.addEventListener(Event.COMPLETE,onComplete)
            loader.addEventListener(IOErrorEvent.IO_ERROR,onComplete)
        }
     
        
        public function detect_faceId(face_id:String, type:String=PType.P25):void
        {
            // TODO Auto Generated method stub
            _type=type
        }
        
        public function detect_img(img:*, type:String=PType.P25):void
        {
            // TODO Auto Generated method stub
            _type=type
        }
      
        
       
        
        
        
        private function onComplete(e:Event):void{
            var loader:URLLoader=e.target as URLLoader;
            loader.removeEventListener(Event.COMPLETE,onComplete)
            loader.removeEventListener(IOErrorEvent.IO_ERROR,onComplete)
            
            if(e.type==Event.COMPLETE){
                _json=String(loader.data)
               
            }else{
                //trace("error");
            }
           
            loader=null;
            dispatchEvent(e)
        }
        
    }
}