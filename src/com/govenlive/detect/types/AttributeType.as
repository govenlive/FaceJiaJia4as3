package com.govenlive.detect.types
{
    
    
    /**
    * 属性type
     *@author 偷心枫贼
     *        下午9:34:01
     *
     */
    public class AttributeType
    {
       // gender, age, race, smiling, glass, pose
       public static const NONE:String="none";
       
       public static const GENDER:String="gender"
           
       public static const AGE:String="age";
       
       public static const RACE:String="race";
       
       public static const SMILING:String="smiling";
       
       public static const GLASS:String="glass";
       
       public static const POSE:String="pose"
           
           
        public static const NORMAL_LABELS:String="gender,age,race,smiling"
    }
}