package com.govenlive.detect.types
{
    
    
    /**
    * face detect api 的modeType
     *@author 偷心枫贼
     *        下午9:30:01
     *
     */
    public class ModeType
    {
        /**
         *默认模式 
         */        
        public static const NORMAL:String="normal";
        /**
         *只找出最大的一张脸 
         */        
        public static const ONEFACE:String="oneface";
    }
}