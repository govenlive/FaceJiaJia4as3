package com.govenlive.detect
{
    import com.govenlive.detect.datas.Face;
    import com.govenlive.detect.event.DetectUpdateEvent;
    import com.govenlive.detect.loaders.ILoaderFaceDetect;
    import com.govenlive.detect.loaders.LoaderFaceDetectFromByte;
    import com.govenlive.detect.loaders.LoaderFaceDetectFromUrl;
    import com.govenlive.detect.types.AttributeType;
    import com.govenlive.detect.types.ModeType;
    
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.IOErrorEvent;
    import flash.utils.ByteArray;
    
    
    [Event(name="update_detect", type="com.govenlive.detect.event.DetectUpdateEvent")]
    [Event(name="ioError", type="flash.events.IOErrorEvent")]
    
    /**
     * 检测一张照片中的人脸信息（脸部位置、年龄、种族、性别等等） 
     * @author Administrator
     * 
     */	
    public class Loader_faceDetect extends EventDispatcher
    {
        private static var _instance:Loader_faceDetect=null;
        
        /**
         * 实现单例化 
         * @return 
         * 
         */        
        public static function get instance():Loader_faceDetect{
            if(_instance==null){
                _instance=new Loader_faceDetect()
            }
            return _instance;
        }
        
        //////////////////////////////////////
        
        public var max_leng:uint=100;
        
        private var _loaders:Vector.<ILoaderFaceDetect>=new Vector.<ILoaderFaceDetect>();
        
        
        
        public function Loader_faceDetect()
        {
            
        }
        
        
        
        /**
         * 获取face信息(注意，是异步的，请监听相应的事件)
         * @param img  图片链接或者图片转成的二进制数据
         * @param mode 检测模式可以是normal(默认) 或者 oneface 。在oneface模式中，检测器仅找出图片中最大的一张脸。
         * @param attribute 可以是none或者由逗号分割的属性列表。默认为gender, age, race, smiling。目前支持的属性包括：gender, age, race, smiling, glass, pose
         * @param tag 可以为图片中检测出的每一张Face指定一个不包含^@,&=*'"等非法字符且不超过255字节的字符串作为tag，tag信息可以通过 /info/get_face 查询
         * @param async 如果置为true，该API将会以异步方式被调用；也就是立即返回一个session id，稍后可通过/info/get_session查询结果。默认值为false。
         * 
         */        
        public function detect(img:*,mode:String=ModeType.NORMAL,attribute:String=AttributeType.NORMAL_LABELS,tag:String="",async:Boolean=false):void{
            if(_loaders.length<max_leng){
                var iloader:ILoaderFaceDetect;
                if(img is String){
                    iloader=new LoaderFaceDetectFromUrl(mode,attribute,tag,async)
                    // getFaceUrl(img,mode,attribute,tag,async);
                }else if(img is ByteArray){
                    //   getFaceByte(img,mode,attribute,tag,async);
                    iloader=new LoaderFaceDetectFromByte(mode,attribute,tag,async)
                }	
                
                iloader.addEventListener(Event.COMPLETE,onComplete)
                iloader.addEventListener(IOErrorEvent.IO_ERROR,onComplete)
                _loaders.push(iloader);
                iloader.load(img)
            }           
        }
        
        
        
        private function onComplete(e:Event):void{
            var loader:ILoaderFaceDetect=e.target as ILoaderFaceDetect
            loader.removeEventListener(Event.COMPLETE,onComplete)
            loader.removeEventListener(IOErrorEvent.IO_ERROR,onComplete)
            _loaders.splice(_loaders.indexOf(loader),1)
            if(e.type==Event.COMPLETE){ 
                var obj:Object=JSON.parse(loader.json);
                var faces:Vector.<Face>=new Vector.<Face>()                  
                var faceArr:Array=obj.face;    
                if(faceArr!=null){
                    for(var i:int=0;i<faceArr.length;i++){
                        var face:Face=new Face()
                        face.copyObject(faceArr[i])
                        faces.push(face)
                    }
                }
                
                
                var img_height:Number=obj.img_height
                var img_width:Number=obj.img_width
                var img_id:String=obj.img_id
                var session_id:String=obj.session_id
                var url:String=obj.url;
                
                var event:DetectUpdateEvent=new DetectUpdateEvent(DetectUpdateEvent.UPDATE_DETECT,false,false,
                    faces,img_width,img_height,img_id,session_id,url)
                
                dispatchEvent(event);
                
            }else{
                //trace("error");
                //  dispatchEvent(new IOErrorEvent(IOErrorEvent.IO_ERROR))
            }
            
            loader=null;
        }
        
        
        
    }
}
