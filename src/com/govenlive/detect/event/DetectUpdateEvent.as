package com.govenlive.detect.event
{
    
    import com.govenlive.detect.datas.Face;
    
    import flash.events.Event;
    
    
    /**
     *@author 偷心枫贼
     *        下午5:03:37
     *
     */
    public class DetectUpdateEvent extends Event
    {
        public static const UPDATE_DETECT:String="update_detect";
        
            
        private var _faces:Vector.<Face>=null;
        private var _img_width:Number=NaN
        private var _img_height:Number=NaN
        private var _img_id:String=""
        private var _session_id:String=""
        private var _url:String=""
            
        /**
         * 脸部集合 
         */  
        public function get faces():Vector.<Face>{return _faces}
        /**
         *请求图片的高度 
         */	
        public function get img_width():Number{return _img_width}
        /**
         *请求图片的宽度 
         */	
        public function get img_height():Number{return _img_height}
        /**
         *Face++系统中的图片标识符，用于标识用户请求中的图片 
         */	  
        public function get img_id():String{return _img_id}   
        /**
         *相应请求的session标识符，可用于结果查询 
         */	
        public function get session_id():String{return _session_id}     
        /**
         *请求中图片的url 
         */	
        public function get url():String{return _url}
        
        
        public function DetectUpdateEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false,
         $faces:Vector.<Face>=null,$img_width:Number=NaN,$img_height:Number=NaN,$img_id="",$session_id:String="",$url:String="" 
         )
        {
            _faces=$faces
            _img_width=$img_width
            _img_height=$img_height
            _img_id=$img_id
            _session_id=$session_id
            _url=$url
            super(type, bubbles, cancelable);
        }
    }
}