package com.govenlive.detect.datas
{
	import com.govenlive.detect.datas.face.Attribute;
	import com.govenlive.detect.datas.face.Position;
	
	
	public class Face
	{
		/**
		 * 人脸属性 
		 */		
		public var attribute:Attribute=new Attribute();
		/**
		 *被检测出的每一张人脸都在Face++系统中的标识符 
		 */		
		public var face_id:String
		/**
		 * 人脸器官的坐标 
		 */		
		public var position:Position=new Position()
		/**
		 *可以为图片中检测出的每一张Face指定一个不包含^@,&=*'"等非法字符且不超过255字节的字符串作为tag，tag信息可以通过 /info/get_face 查询 
		 */		
		public var tag:String
		
		public function Face()
		{
			
		}
		
		
		public function copyObject(obj:Object):void{
			
			attribute.copyObject(obj.attribute);
			face_id=obj.face_id;
			position.copyObject(obj.position);
			tag=obj.tag
		}
		
		
		
		
	}
}
