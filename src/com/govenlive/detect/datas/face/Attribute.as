package com.govenlive.detect.datas.face
{
	import com.govenlive.detect.datas.face.attribute.Age;
	import com.govenlive.detect.datas.face.attribute.Gender;
	import com.govenlive.detect.datas.face.attribute.Glass;
	import com.govenlive.detect.datas.face.attribute.Pose;
	import com.govenlive.detect.datas.face.attribute.Race;
	import com.govenlive.detect.datas.face.attribute.Smiling;

	/**
	 * 脸部属性 
	 * @author goven225
	 * 
	 */	
	public class Attribute
	{
		/**
		 *包含年龄分析结果 
		 */		
		public var age:Age=new Age()
		/**
		 *包含性别分析结果 
		 */			
		public var gender:Gender=new Gender()
		/**
		 *包含眼镜佩戴分析结果 
		 */			
		public var glass:Glass=new Glass()
		/**
		 *包含脸部姿势分析结果 
		 */			
		public var pose:Pose=new Pose()
		/**
		 *包含人种分析结果 
		 */			
		public var race:Race=new Race()
		/**
		 *包含微笑程度分析结果 
		 */			
		public var smiling:Smiling=new Smiling();
		
		public function Attribute()
		{
		}
		
		public function copyObject(obj:Object):void{
			age.copyObject(obj.age)
			gender.copyObject(obj.gender)
			glass.copyObject(obj.glass);
		
			pose.copyObject(obj.pose);
			race.copyObject(obj.race)
			smiling.copyObject(obj.smiling);
		}
		
		
		public function toString():String{
			
			
			
			
			return age.toString()+"\n" +
				""+gender.toString()+"\n" +
				""+race.toString()+"\n" +
				""+smiling.toString()+"\n" +
				""+glass.toString()+"\n" +
				""+pose.toString();
		}
	}
}