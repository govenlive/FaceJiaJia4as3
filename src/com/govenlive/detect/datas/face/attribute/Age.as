package com.govenlive.detect.datas.face.attribute
{
	/**
	 * 包含年龄分析结果，value的值为一个非负整数表示估计的年龄, range表示估计年龄的正负区间
	 * @author goven225
	 * 
	 */	
	public class Age extends __attribute
	{
		/**
		 * 表示估计年龄的正负区间
		 */		
		public var range:int 
		/**
		 * 非负整数表示估计的年龄
		 */		
		public var value:uint 
		
		public function Age()
		{
		}
		
		override public function copyObject(obj:Object):void
		{
			// TODO Auto Generated method stub
			super.copyObject(obj);
			range=obj.range
			value=obj.value
		}
		
		override public function toString():String
		{
			// TODO Auto Generated method stub
			return "年龄: "+(value-range).toString()+"-"+(value+range).toString()+" 岁."
		}
		
		
	}
}