package com.govenlive.detect.datas.face.attribute
{
	/**
	 * 包含眼镜佩戴分析结果，value的值为None/Dark/Normal, confidence表示置信度 
	 * @author goven225
	 * 
	 */	
	public class Glass extends __attribute
	{
		public static const NONE:String="None"
		public static const Dark:String="Dark";  //黑 暗
		public static const Normal:String="Normal" //正常
		
		
		/**
		 * 置信度  
		 */		
		public var confidence:Number
		/**
		 * 值为None/Dark/Normal
		 */		
		public var value:String
		
		
		
		public function Glass()
		{
		}
		
		override public function copyObject(obj:Object):void
		{
			// TODO Auto Generated method stub
			super.copyObject(obj);
			if(obj){
				confidence=obj.confidence
				value=obj.value;
			}
			
		}
		
		override public function toString():String
		{
			if(value==NONE){
				return "您未佩戴眼镜"
			}else{
				return "您正戴着眼镜"
			}
			
		}
		
		
		
	}
}