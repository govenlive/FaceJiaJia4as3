package com.govenlive.detect.datas.face.attribute
{
	/**
	 *包含微笑程度分析结果，value的值为0－100的实数，越大表示微笑程度越高 
	 * @author goven225
	 * 
	 */	
	public class Smiling extends __attribute
	{
		/**
		 * 微笑程度  0-100; 
		 */		
		public var value:Number
		public function Smiling()
		{
		}
		
		override public function copyObject(obj:Object):void
		{
			// TODO Auto Generated method stub
			super.copyObject(obj);
			value=obj.value;
		}
		
		
		override public function toString():String
		{
			
			// TODO Auto Generated method stub
			return "微笑指数 : "+value.toFixed(2)+"%";
		}
		
	}
}