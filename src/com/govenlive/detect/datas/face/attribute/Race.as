package com.govenlive.detect.datas.face.attribute
{
	/**
	 * 包含人种分析结果，value的值为Asian/White/Black, confidence表示置信度 
	 * @author goven225
	 * 
	 */	
	public class Race extends __attribute
	{
		public static const ASIAN:String="Asian"//亚洲人
		public static const White:String="White" //白种人
		public static const Black:String="Black" //黑种人
		
		public var confidence:Number  // 置信度
		public var value:String //值
		public function Race()
		{
		}
		
		override public function copyObject(obj:Object):void
		{
			// TODO Auto Generated method stub
			super.copyObject(obj);
			confidence=obj.confidence
			value=obj.value;
		}
		private function getCn():String{
			if(value==ASIAN)return "黄种人";
			if(value==White)return "白种人"
			if(value==Black)return "黑种人"
			return "外星人"
		}
		
		override public function toString():String
		{
			// TODO Auto Generated method stub
			return "属于 : "+getCn();
		}
		
		
	}
}