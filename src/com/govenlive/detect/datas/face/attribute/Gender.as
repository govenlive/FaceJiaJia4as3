package com.govenlive.detect.datas.face.attribute
{
	/**
	 * 包含性别分析结果，value的值为Male/Female, confidence表示置信度 
	 * @author goven225
	 * 
	 */	
	public class Gender extends __attribute
	{
		public static const MALE:String="Male";//男性
		public static const FEMALE:String="Female"//女性
		
		
		/**
		 * 置信度  
		 */		
		public var confidence:Number
		/**
		 * 值为Male/Female 
		 */		
		public var value:String
		public function Gender()
		{
		}
		private function getCn():String{
			if(value==MALE)return "男人"
			if(value==FEMALE)return "女人"
			return "人妖"
		}
		override public function copyObject(obj:Object):void
		{
			// TODO Auto Generated method stub
			super.copyObject(obj);
			
			confidence=obj.confidence
			value=obj.value;
		}
		
		override public function toString():String
		{
			// TODO Auto Generated method stub
			return "性别 : "+getCn();
		}
		
	}
}