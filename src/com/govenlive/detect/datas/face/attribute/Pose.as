package com.govenlive.detect.datas.face.attribute
{
	/**
	 * 包含脸部姿势分析结果，包括pitch_angle, roll_angle, yaw_angle，分别对应抬头，旋转（平面旋转），摇头。单位为角度。 
	 * @author goven225
	 * 
	 */	
	public class Pose extends __attribute
	{
		
		public var pitch_angle:Angel=new Angel()  //抬头
		public var roll_angle:Angel=new Angel() //旋转
		public var yaw_angle:Angel=new Angel() //摇头
		
		
		public function Pose()
		{
			
		}
		
		override public function copyObject(obj:Object):void
		{
			
			// TODO Auto Generated method stub
			super.copyObject(obj);
			if(obj){
				pitch_angle.value=obj.pitch_angle.value;
				roll_angle.value=obj.roll_angle.value;
				yaw_angle.value=obj.yaw_angle.value;
			}
			
		}
		
		override public function toString():String
		{
			// TODO Auto Generated method stub
			return "抬头角度: "+pitch_angle.value+"度，  歪头角度: "+roll_angle.value+"度,  摇头角度: "+yaw_angle.value
		}
		
		
		
	}
}


class Angel {
	
	public var value:Number
	
}