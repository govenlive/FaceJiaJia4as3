package com.govenlive.detect.datas.face
{
	import com.govenlive.detect.datas.face.position.Center;
	import com.govenlive.detect.datas.face.position.Eye_left;
	import com.govenlive.detect.datas.face.position.Eye_right;
	import com.govenlive.detect.datas.face.position.Mouth_left;
	import com.govenlive.detect.datas.face.position.Mouth_right;
	import com.govenlive.detect.datas.face.position.Nose;
	
	/**
	 * 坐标 
	 * @author goven225
	 * 
	 */	
	public class Position
	{
		/**
		 *0~100之间的实数，表示检出的脸的宽度在图片中百分比 
		 */		
		public var width:Number
		/**
		 *0~100之间的实数，表示检出的脸的高度在图片中百分比 
		 */		
		public var height:Number
		
		/**
		 *检出的人脸框的中心点坐标 
		 */		
		public var center:Center=new Center()
		/**
		 *相应人脸的左眼坐标 
		 */			
		public var eye_left:Eye_left=new Eye_left()
		/**
		 *相应人脸的右眼坐标 
		 */			
		public var eye_right:Eye_right=new Eye_right()
		/**
		 *相应人脸的左侧嘴角坐标 
		 */			
		public var mouth_left:Mouth_left=new Mouth_left()
		/**
		 *相应人脸的右侧嘴角坐标 
		 */			
		public var mouth_right:Mouth_right=new Mouth_right()
		/**
		 *相应人脸的鼻尖坐标 
		 */			
		public var nose:Nose=new Nose()
		public function Position()
		{
			
		}
		public function copyObject(obj:Object):void{
			center.copyObject(obj.center);
			eye_left.copyObject(obj.eye_left)
			eye_right.copyObject(obj.eye_right)
			mouth_left.copyObject(obj.mouth_left)
			mouth_right.copyObject(obj.mouth_right)
			nose.copyObject(obj.nose);	
			
			width=obj.width
			height=obj.height
		}
	}
}