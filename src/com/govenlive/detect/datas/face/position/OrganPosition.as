package com.govenlive.detect.datas.face.position
{
	/**
	 * 器官 坐标
	 * @author goven225
	 * 
	 */	
	public class OrganPosition
	{
		/**
		 * 在图片中的宽度的百分比（0-100） 
		 */		
		public var x:Number
		/**
		 *在图片中的高度百分比(0-100); 
		 */		
		public var y:Number
		
		
		
		public function get p_x():Number{
			return x/100;
		}
		
		public function get p_y():Number{
			return y/100;
		}
		public function OrganPosition()
		{
		}
		
		/**
		 * 
		 * @param obj
		 * 
		 */		
		public function copyObject(obj:Object):void{
			x=obj.x
			y=obj.y;
		}
			
	}
}