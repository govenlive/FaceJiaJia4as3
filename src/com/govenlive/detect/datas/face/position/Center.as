package com.govenlive.detect.datas.face.position
{
	/**
	 * 检出的人脸框的中心点坐标 
	 * @author goven225
	 * 
	 */	
	public class Center extends OrganPosition
	{
		public function Center()
		{
			super();
		}
	}
}