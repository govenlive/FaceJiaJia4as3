package com.govenlive.detect.datas.face.position
{
	/**
	 * 相应人脸的右侧嘴角坐标 
	 * @author goven225
	 * 
	 */	
	public class Mouth_right extends OrganPosition
	{
		public function Mouth_right()
		{
			super();
		}
	}
}