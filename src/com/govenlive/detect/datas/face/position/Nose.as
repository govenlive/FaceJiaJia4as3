package com.govenlive.detect.datas.face.position
{
	/**
	 *相应人脸的鼻尖坐标 
	 * @author goven225
	 * 
	 */	
	public class Nose extends OrganPosition
	{
		public function Nose()
		{
			super();
		}
	}
}