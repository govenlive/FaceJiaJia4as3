package com.govenlive.detect.datas.face.position
{
	/**
	 * 相应人脸的左侧嘴角坐标 
	 * @author goven225
	 * 
	 */	
	public class Mouth_left extends OrganPosition
	{
		public function Mouth_left()
		{
			super();
		}
	}
}