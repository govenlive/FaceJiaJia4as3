package com.govenlive.detect.loaders
{
    import com.govenlive._g.UrlTypes;
    import com.govenlive.detect.types.AttributeType;
    import com.govenlive.detect.types.ModeType;
    
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.net.URLLoader;
    import flash.net.URLRequest;
    import flash.net.URLRequestMethod;
    import flash.net.URLVariables;
    
    
    /**
     *@author 偷心枫贼
     *        下午10:20:09
     *
     */
    public class LoaderFaceDetectFromUrl extends _LoaderFace
    {
        public function LoaderFaceDetectFromUrl(mode:String=ModeType.NORMAL, attribute:String=AttributeType.NORMAL_LABELS, tag:String="", async:Boolean=false)
        {
            super(mode, attribute, tag, async);   
        }
        
        override public function load(img:*):void
        {
            // TODO Auto Generated method stub
            super.load(img);        
            variables.url=img as String
                         
            req.data = variables
            req.method = URLRequestMethod.GET;

            loader.load(req);
        }
        
        
        
    }
}