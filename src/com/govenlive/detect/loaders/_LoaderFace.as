package com.govenlive.detect.loaders
{
    
    import com.govenlive.FaceLibConfig;
    import com.govenlive._g.UrlTypes;
    import com.govenlive.detect.Loader_faceDetect;
    import com.govenlive.detect.datas.Face;
    import com.govenlive.detect.event.DetectUpdateEvent;
    import com.govenlive.detect.types.AttributeType;
    import com.govenlive.detect.types.ModeType;
    
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.IOErrorEvent;
    import flash.net.URLLoader;
    import flash.net.URLRequest;
    import flash.net.URLVariables;
    
    
    /**
     *@author 偷心枫贼
     *        下午10:17:27
     *
     */
    internal class _LoaderFace extends EventDispatcher  implements ILoaderFaceDetect
    {
        protected var loader:URLLoader=new URLLoader()
        protected var req:URLRequest=new URLRequest();
        protected var variables:URLVariables = new URLVariables(); 
        
        private var _json:String=""
            
            
            
        /**
         * json 
         * @return 
         * 
         */            
        public function get json():String{return _json}
        
        public function _LoaderFace(mode:String=ModeType.NORMAL,attribute:String=AttributeType.NORMAL_LABELS,tag:String="",async:Boolean=false)
        {
            super();
 
            variables.api_key=FaceLibConfig.api_key
            variables.api_secret=FaceLibConfig.api_secret
            variables.mode=mode
            variables.attribute=attribute
            variables.tag=tag
            variables.async=async   
                
                
            req.url=UrlTypes.DETECT_URL
            loader.addEventListener(Event.COMPLETE,onComplete)
            loader.addEventListener(IOErrorEvent.IO_ERROR,onComplete)
        }
        
        public function load(img:*):void
        {
            // TODO Auto Generated method stub
            
        }
        
      
        
      
        protected  function onComplete(e:Event):void{
            var loader:URLLoader=e.target as URLLoader;
            loader.removeEventListener(Event.COMPLETE,onComplete)
            loader.removeEventListener(IOErrorEvent.IO_ERROR,onComplete)
            if(e.type==Event.COMPLETE){                
                _json=loader.data       
            }
            dispatchEvent(e)
            loader=null;
        }
    }
}