package com.govenlive.detect.loaders
{
    import com.govenlive._g.UrlTypes;
    import com.govenlive.detect.types.AttributeType;
    import com.govenlive.detect.types.ModeType;
    import com.net.upload.UploadPostHelper;
    
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.net.URLLoader;
    import flash.net.URLRequest;
    import flash.net.URLRequestHeader;
    import flash.net.URLRequestMethod;
    import flash.net.URLVariables;
    import flash.utils.ByteArray;
    
    
    /**
     *@author 偷心枫贼
     *        下午10:24:42
     *
     */
    public class LoaderFaceDetectFromByte extends _LoaderFace
    {
        public function LoaderFaceDetectFromByte(mode:String=ModeType.NORMAL, attribute:String=AttributeType.NORMAL_LABELS, tag:String="", async:Boolean=false)
        {
            super(mode, attribute, tag, async);
        }
        
        override public function load(img:*):void
        {
            // TODO Auto Generated method stub
            super.load(img);         
            variables.img=img as ByteArray

            req.method=URLRequestMethod.POST;
            req.data=UploadPostHelper.getPostData("img.jpg",img,"img",variables);
            req.requestHeaders.push(new URLRequestHeader('Cache-Control', 'no-cache'));
            req.requestHeaders.push(new URLRequestHeader('Content-Type', 'multipart/form-data; boundary=' + UploadPostHelper.getBoundary()));
            
            loader.load(req);
        }
        
        
    }
}