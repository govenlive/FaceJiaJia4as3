package com.govenlive.detect.loaders
{
    import flash.events.IEventDispatcher;
    
    
    /**
     *@author 偷心枫贼
     *        下午10:24:11
     *
     */
    public interface ILoaderFaceDetect extends IEventDispatcher
    {
        function load(img:*):void
            
        function get json():String
    }
}